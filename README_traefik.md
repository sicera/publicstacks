Before you can start the traefik stack, you need to create swarm network:

```
docker network create -d overlay --scope swarm traefik-prod
```